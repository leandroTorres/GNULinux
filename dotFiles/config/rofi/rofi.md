cp solarized.rasi /usr/share/rofi/themes/
rofi -combi-modi window,drun,filebrowser -icon-theme Numix -show-icons -theme solarized -show drun
