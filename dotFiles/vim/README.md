# Configuraciones para vim

## Agregar en /etc/vim/

1. vimrc

Las configuraciones que se deben habilitar aqui son:
```
syntax on
set background=dark
filetype plugin indent on
```

y se deben agregar:
```
" Source a global configuration file if available
if filereadable("/etc/vim/latexsuite.local")
  source /etc/vim/latexsuite.local
endif

" vim LaTeX suite
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
```

2. vimrc.local

3. latexsuite.local

