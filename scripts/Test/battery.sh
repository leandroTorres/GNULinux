#!/usr/bin/bash

readarray -t output <<< $(sudo tlp-stat -b)
battery_count=1

for line in "${output[21]}";
do
    percentages+=($(echo "$line" | grep -o -m1 '[0-9]\{1,3\}%' | tr -d '%'))
    statuses+=($(echo "$line" | egrep -o -m1 'Discharging|Charge|AC|Full|Unknown'))
    remaining=$(echo "$line" | egrep -o -m1 '[0-9][0-9]:[0-9][0-9]')
    if [[ -n $remaining ]]; then
        remainings+=(" ($remaining)")
    else 
        remainings+=("")
    fi
done

echo $percentages

