#!/usr/bin/bash

case "$1" in
    on)
        echo "turn on second monitor on the left  - - - - - - - - - - - - - -"

        xrandr --output HDMI2 --auto --noprimary --right-of LVDS1
        ;;
    off)
        echo "turn off second monitor - - - - - - - - - - - - - - - - - - - -"
        xrandr --auto && xrandr --output HDMI2 --off
        ;;
    *)
        echo "opciones: on or off"
        ;;
esac
